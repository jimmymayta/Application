-- \i E:/ProjectsPython/Linxs/LinxsDatabase.sql


Jimmy Mayta

JIMMY MAYTA

J1MMYM4YT4

CREATE USER "Lazarus" WITH PASSWORD 'L4Z4RU5';
ALTER ROLE "Lazarus" WITH SUPERUSER;

CREATE DATABASE "Database" WITH OWNER = "Database" ENCODING = 'UTF8' CONNECTION LIMIT = -1;
GRANT ALL PRIVILEGES ON DATABASE "Database" TO "Mayta" WITH GRANT OPTION;
\connect Database Database localhost 5432

\connect postgres postgres localhost 5432
DROP DATABASE "LinxsDatabase";

CREATE DATABASE "Pruebas" WITH OWNER = "postgres" ENCODING = 'UTF8' CONNECTION LIMIT = -1;
\connect "Pruebas" postgres localhost 5432

CREATE TABLE public."Usuarios" (
    "IDUsu" BIGINT NOT NULL,
    "UsuNombres" CHARACTER VARYING(128) NOT NULL,
    "UsuApellidos" CHARACTER VARYING(128) NOT NULL,
    "UsuUsuario" CHARACTER VARYING(128) NOT NULL,
    "UsuContrasena" CHARACTER VARYING(128) NOT NULL,
    "UsuCorreoElectronico" CHARACTER VARYING(128) NOT NULL,
    "UsuFechaCreacion" TIMESTAMP(0) NOT NULL,
    "UsuFechaActualizacion" TIMESTAMP(0) NOT NULL,
    "UsuIDUsu" BIGINT NOT NULL,
    CONSTRAINT "PKIDUsu" PRIMARY KEY ("IDUsu"),
    CONSTRAINT "FKUsuIDUsu" FOREIGN KEY ("UsuIDUsu")
    REFERENCES public."Usuarios" ("IDUsu")
);
INSERT INTO public."Usuarios" VALUES (
    1, 'Jimmy', 'Mayta', 'jimmy', '123', 'jm@gmail.com', '2020-06-14', '2020-06-14', 1
);
INSERT INTO public."Usuarios" VALUES (
    2, 'Eva', 'Montesco', 'eva', '1234', 'eva@gmail.com', '2020-06-15', '2020-06-15', 1
);
INSERT INTO public."Usuarios" VALUES (
    3, 'Linda', 'Montesco', 'linda', '12345', 'linda@gmail.com', '2020-06-16', '2020-06-16', 2
);

INSERT INTO public."Usuarios" VALUES (
    4, 'Estrella', 'Montesco', 'estrella', '123', 'estrella@gmail.com', '2020-06-17', '2020-06-17', 5
);





CREATE TABLE public."Usuarios" (
    "IDUsu" BIGINT NOT NULL,
    "UsuUsuario" CHARACTER VARYING(128) NOT NULL,
    "UsuContrasena" CHARACTER VARYING(128) NOT NULL,
    "UsuFechaCreAct" TIMESTAMP(0) NOT NULL,
    "UsuIDUsu" BIGINT NOT NULL,
    CONSTRAINT "PKIDUsu" PRIMARY KEY ("IDUsu"),
    CONSTRAINT "FKUsuIDUsu" FOREIGN KEY ("UsuIDUsu")
    REFERENCES public."Usuarios" ("IDUsu")
);





CREATE TABLE public."Admin" (
    "IDAdm" BIGINT NOT NULL,
    "AdmNombres" CHARACTER VARYING(128) NOT NULL,
    "AdmApellidos" CHARACTER VARYING(128) NOT NULL,
    "AdmUsuario" CHARACTER VARYING(128) NOT NULL,
    "AdmContrasena" CHARACTER VARYING(128) NOT NULL,
    "AdmCorreoElectronico" CHARACTER VARYING(128) NOT NULL,
    "AdmFechaCreacion" TIMESTAMP(0) NOT NULL,
    "AdmFechaActualizacion" TIMESTAMP(0) NOT NULL,
    CONSTRAINT "PKIDAdm" PRIMARY KEY ("IDAdm")
);
INSERT INTO public."Admin"
VALUES (1, 'Eva', 'Montesco', 'Eva', '123', 'eva@gmil.com', (SELECT current_timestamp), (SELECT current_timestamp));


CREATE TABLE public."Generos" (
    "IDGen" BIGINT NOT NULL,
    "GenGenero" CHARACTER VARYING(1024) NOT NULL,
    "GenDescripcion" CHARACTER VARYING(1024) NOT NULL,
    "GenIDAdm" BIGINT NOT NULL,
    "GenFechaCreacion" TIMESTAMP(0) NOT NULL,
    "GenFechaActualizacion" TIMESTAMP(0) NOT NULL,
    CONSTRAINT "PKIDGen" PRIMARY KEY ("IDGen"),
    CONSTRAINT "FKGenIDAdm" FOREIGN KEY ("GenIDAdm")
    REFERENCES public."Admin" ("IDAdm")
);
INSERT INTO public."Generos" VALUES (1, 'Mujer', 'Damas', 1, (SELECT current_timestamp), (SELECT current_timestamp));
INSERT INTO public."Generos" VALUES (2, 'Hombre', 'Caballeros', 1, (SELECT current_timestamp), (SELECT current_timestamp));


CREATE TABLE public."Estados" (
    "IDEst" BIGINT NOT NULL,
    "EstEstado" CHARACTER VARYING(1024) NOT NULL,
    "EstDescripcion" CHARACTER VARYING(1024) NOT NULL,
    "EstIDAdm" BIGINT NOT NULL,
    "EstFechaCreacion" TIMESTAMP(0) NOT NULL,
    "EstFechaActualizacion" TIMESTAMP(0) NOT NULL,
    CONSTRAINT "PKIDEst" PRIMARY KEY ("IDEst"),
    CONSTRAINT "FKEstIDAdm" FOREIGN KEY ("EstIDAdm")
    REFERENCES public."Admin" ("IDAdm")
);
CREATE SEQUENCE "SecEstados"
START WITH 1
INCREMENT BY 1
MINVALUE 1;
CREATE PROCEDURE "Estados" (Est VARCHAR(1024), Descripcion VARCHAR(1024), IDAdm INTEGER)
LANGUAGE plpgsql
AS $$
BEGIN
    INSERT INTO public."Estados" VALUES (NEXTVAL('"SecEstados"'), Est, Descripcion, IDAdm, (SELECT current_timestamp), (SELECT current_timestamp));
END;
$$;
CALL "Estados"('Activado', 'Activado, que esta activo', 1);
CALL "Estados"('Desactivado', 'Desactivado, que esta iancivo o eliminado', 1);


CREATE TABLE public."Departamentos" (
    "IDDep" BIGINT NOT NULL,
    "DepAbreviatura" CHARACTER VARYING(1024) NOT NULL,
    "DepDepartamento" CHARACTER VARYING(1024) NOT NULL,
    "DepCodigo" CHARACTER VARYING(1024) NOT NULL,
    "DepIDAdm" BIGINT NOT NULL,
    "DepFechaCreacion" TIMESTAMP(0) NOT NULL,
    "DepFechaActualizacion" TIMESTAMP(0) NOT NULL,
    CONSTRAINT "PKIDDep" PRIMARY KEY ("IDDep"),
    CONSTRAINT "FKDepIDAdm" FOREIGN KEY ("DepIDAdm")
    REFERENCES public."Admin" ("IDAdm")
);
INSERT INTO public."Departamentos" VALUES (1,'BE','Beni','08',1,(SELECT current_timestamp), (SELECT current_timestamp));
INSERT INTO public."Departamentos" VALUES (2,'CH','Chuquisaca','01',1,(SELECT current_timestamp), (SELECT current_timestamp));
INSERT INTO public."Departamentos" VALUES (3,'CB','Cochabamba','03',1, (SELECT current_timestamp), (SELECT current_timestamp));
INSERT INTO public."Departamentos" VALUES (4,'LP','La Paz','02',1,(SELECT current_timestamp), (SELECT current_timestamp));
INSERT INTO public."Departamentos" VALUES (5,'OR','Oruro','04',1,(SELECT current_timestamp), (SELECT current_timestamp));
INSERT INTO public."Departamentos" VALUES (6,'PD','Pando','09',1,(SELECT current_timestamp), (SELECT current_timestamp));
INSERT INTO public."Departamentos" VALUES (7,'PT','Potosí','05',1,(SELECT current_timestamp), (SELECT current_timestamp));
INSERT INTO public."Departamentos" VALUES (8,'SC','Santa Cruz','07',1,(SELECT current_timestamp), (SELECT current_timestamp));
INSERT INTO public."Departamentos" VALUES (9,'TJ','Tarija','06',1,(SELECT current_timestamp), (SELECT current_timestamp));




CREATE TABLE public."Permisos" (
    "IDPer" BIGINT NOT NULL,
    "PerPermiso" CHARACTER VARYING(1024) NOT NULL,
    "PerDescripcion" CHARACTER VARYING(1024) NOT NULL,
    "PerIDAdm" BIGINT NOT NULL,
    "PerFechaCreacion" TIMESTAMP(0) NOT NULL,
    "PerFechaActualizacion" TIMESTAMP(0) NOT NULL,
    CONSTRAINT "PKIDPer" PRIMARY KEY ("IDPer"),
    CONSTRAINT "FKPerIDAdm" FOREIGN KEY ("PerIDAdm")
    REFERENCES public."Admin" ("IDAdm")
);
CREATE SEQUENCE "SecPermisos"
START WITH 1
INCREMENT BY 1
MINVALUE 1;

CREATE PROCEDURE "Permisos" (Per VARCHAR(1024), Descripcion VARCHAR(1024), IDAdm INTEGER)
LANGUAGE plpgsql
AS $$
BEGIN
    INSERT INTO public."Permisos"
    VALUES (NEXTVAL('"SecPermisos"'), Per, Descripcion, IDAdm, (SELECT current_timestamp), (SELECT current_timestamp));
END;
$$;

CALL "Permisos"('SuperAdmin', 'Todos los permisos. Solo uno debe tener.', 1);
CALL "Permisos"('Admin', 'Permisos de administrador.', 1);
CALL "Permisos"('Programadores', 'Tiene acceso a ciertas tablas', 1);
CALL "Permisos"('Ventas', 'Permiso para ventas. Solo puede crear y leer, no puede eliminar', 1);
CALL "Permisos"('Limpieza', 'Tiene acceso a ciertas tablas', 1);


CREATE TABLE public."Personal" (
    "IDPer" BIGINT NOT NULL,
    "PerCI" CHARACTER VARYING(1024) NOT NULL,
    "PerIDDep" BIGINT NOT NULL,
    "PerNombres" CHARACTER VARYING(1024) NOT NULL,
    "PerApellidos"  CHARACTER VARYING(1024) NOT NULL,
    "PerUsuario"  CHARACTER VARYING(1024) NOT NULL,
    "PerContrasena" CHARACTER VARYING(1024) NOT NULL,
    "PerIDPer" BIGINT NOT NULL,
    "PerIDEst" BIGINT NOT NULL,
    "PerIDAdm" BIGINT NOT NULL,
    "PerFechaCreacion" TIMESTAMP(0) NOT NULL,
    "PerFechaActualizacion" TIMESTAMP(0) NOT NULL,
    CONSTRAINT "PKIDPersonal" PRIMARY KEY ("IDPer"),
    CONSTRAINT "FKPerIDDep" FOREIGN KEY ("PerIDDep")
    REFERENCES public."Departamentos" ("IDDep"),
    CONSTRAINT "FKPerIDPer" FOREIGN KEY ("PerIDPer")
    REFERENCES public."Permisos" ("IDPer"),
    CONSTRAINT "FKPerIDEst" FOREIGN KEY ("PerIDEst")
    REFERENCES public."Estados" ("IDEst"),
    CONSTRAINT "FKPerIDAdm" FOREIGN KEY ("PerIDAdm")
    REFERENCES public."Admin" ("IDAdm")
);
CREATE SEQUENCE "SecPersonal"
START WITH 1
INCREMENT BY 1
MINVALUE 1;

CREATE PROCEDURE "Personal" (CI VARCHAR(1024),
                             IDDep INTEGER,
                             Nombres VARCHAR(1024),
                             Apellidos VARCHAR(1024),
                             Usuario VARCHAR(1024),
                             Contrasena VARCHAR(1024),
                             IDPer INTEGER,
                             IDEst INTEGER,
                             IDAdm INTEGER)
LANGUAGE plpgsql
AS $$
BEGIN
    INSERT INTO public."Personal"
    VALUES (NEXTVAL('"SecPersonal"'), CI, IDDep, Nombres, Apellidos, Usuario, Contrasena, IDPer, IDEst, IDAdm, (SELECT current_timestamp), (SELECT current_timestamp));
END;
$$;
CALL "Personal"('6913193', 4, 'Jimmy Y.', 'Mayta J.', 'Jimmy', '123', 1, 1, 1);


CREATE TABLE public."DocumentacionPersonal" (
    "IDDocPer" BIGINT NOT NULL,
    "DocPerIDPer" BIGINT NOT NULL,
    "DocPerIDGen" BIGINT NOT NULL,
    "DocPerFechaNacimiento" TIMESTAMP(0) NOT NULL,
    "DocPerCurriculumVitae" CHARACTER VARYING(1024) NOT NULL,
    "DocPerIDAre" BIGINT NOT NULL,
    "DocPerSalario" BIGINT NOT NULL,
    "DocPerFechaCreacion" TIMESTAMP(0) NOT NULL,
    "DocPerFechaActualizacion" TIMESTAMP(0) NOT NULL,
    CONSTRAINT "PKIDDocPer" PRIMARY KEY ("IDDocPer"),
    CONSTRAINT "FKDocPerIDPer" FOREIGN KEY ("DocPerIDPer")
    REFERENCES public."Personal" ("IDPer"),
    CONSTRAINT "FKDocPerIDGen" FOREIGN KEY ("DocPerIDGen")
    REFERENCES public."Generos" ("IDGen"),
    CONSTRAINT "FKDocPerIDAre" FOREIGN KEY ("DocPerIDAre")
    REFERENCES public."Areas" ("IDAre")
);
CREATE SEQUENCE "SecDocPer"
START WITH 1
INCREMENT BY 1
MINVALUE 1;
CREATE PROCEDURE "DocumentacionPersonal" (IDPer INTEGER,
                                          IDGen INTEGER,
                                          FechaNacimiento TIMESTAMP(0),
                                          CurriculumVitae VARCHAR(1024),
                                          IDAre INTEGER,
                                          Salario VARCHAR(1024))
LANGUAGE plpgsql
AS $$
BEGIN
    INSERT INTO public."DocumentacionPersonal"
    VALUES (NEXTVAL('"SecDocPer"'), IDPer, IDGen, FechaNacimiento, CurriculumVitae, IDAre, Salario,
            (SELECT current_timestamp), (SELECT current_timestamp));
END;
$$;
CALL "DocumentacionPersonal"(1, 2, '1999-12-17', 'Si', 1, '3900 Bs');


CREATE TABLE public."Areas" (
    "IDAre" BIGINT NOT NULL,
    "AreArea" CHARACTER VARYING(1024) NOT NULL,
    "AreDescripcion" CHARACTER VARYING(1024) NOT NULL,
    "AreIDPer" BIGINT NOT NULL,
    "AreIDAdm" BIGINT NOT NULL,
    "AreFechaCreacion" TIMESTAMP(0) NOT NULL,
    "AreFechaActualizacion" TIMESTAMP(0) NOT NULL,
    CONSTRAINT "PKIDAre" PRIMARY KEY ("IDAre"),
    CONSTRAINT "FKAreIDPer" FOREIGN KEY ("AreIDPer")
    REFERENCES public."Personal" ("IDPer"),
    CONSTRAINT "FKAreIDAdm" FOREIGN KEY ("AreIDAdm")
    REFERENCES public."Admin" ("IDAdm")
);
CREATE SEQUENCE "SecAreas"
START WITH 1
INCREMENT BY 1
MINVALUE 1;
CREATE PROCEDURE "Areas" (Are VARCHAR(1024), Descripcion VARCHAR(1024), IDPer INTEGER, IDAdm INTEGER)
LANGUAGE plpgsql
AS $$
BEGIN
    INSERT INTO public."Areas"
    VALUES (NEXTVAL('"SecAreas"'), Are, Descripcion, IDPer, IDAdm, (SELECT current_timestamp), (SELECT current_timestamp));
END;
$$;
CALL "Areas"('Desarrollo', 'Area de desarrollo de software', 1, 1);
CALL "Areas"('Ventas', 'Area de ventas de productos', 1, 1);
CALL "Areas"('Limpieza', 'Area de Limpieza', 1, 1);
CALL "Areas"('Contabilidad', 'Area contable', 1, 1);








CREATE TABLE public."Licencias" (
    "IDLic" BIGINT NOT NULL,
    "LicIDLicencia" CHARACTER VARYING(1024) NOT NULL,
    "LicLicencia" CHARACTER VARYING(1024) NOT NULL,
    "LicUsuario" CHARACTER VARYING(1024) NOT NULL,
    "LicCorreoElectronico" CHARACTER VARYING(1024) NOT NULL,
    "LicDescripcion" CHARACTER VARYING(1024) NOT NULL,
    "LicIDEst" BIGINT NOT NULL,
    "LicIDPerm" BIGINT NOT NULL,
    "LicIDPers" BIGINT NOT NULL,
    "LicIDAdm" BIGINT NOT NULL,
    "LicFechaCreacion" TIMESTAMP(0) NOT NULL,
    "LicFechaActualizacion" TIMESTAMP(0) NOT NULL,

    CONSTRAINT "PKIDLic" PRIMARY KEY ("IDLic"),

    CONSTRAINT "FKLicIDEst" FOREIGN KEY ("LicIDEst")
    REFERENCES public."Estados" ("IDEst"),

    CONSTRAINT "FKLicIDPerm" FOREIGN KEY ("LicIDPerm")
    REFERENCES public."Permisos" ("IDPer"),
    
    CONSTRAINT "FKLicIDPers" FOREIGN KEY ("LicIDPers")
    REFERENCES public."Personal" ("IDPer"),
    
    CONSTRAINT "FKLicIDAdm" FOREIGN KEY ("LicIDAdm")
    REFERENCES public."Admin" ("IDAdm")
);

CREATE SEQUENCE "SecLicencias"
START WITH 1
INCREMENT BY 1
MINVALUE 1;
CREATE PROCEDURE "Licencias" (IDLic VARCHAR(1024),
                              Lic VARCHAR(1024),
                              Usu VARCHAR(1024),
                              CorEle VARCHAR(1024),
                              Des VARCHAR(1024),
                              IDEst INTEGER,
                              IDPerm INTEGER,
                              IDPers INTEGER,
                              IDAdm INTEGER)
LANGUAGE plpgsql
AS $$
BEGIN
    INSERT INTO public."Licencias"
    VALUES (NEXTVAL('"SecLicencias"'), IDLic, Lic, Usu, CorEle, Des, IDEst, IDPerm, IDPers, IDAdm,
    (SELECT current_timestamp), (SELECT current_timestamp));
END;
$$;
CALL "Licencias"('3tQVuKqI','27ZQs0MlQZkxg82MWzFxdUPmrU4nsV','Jimmy Mayta','jimmyymaytaj@gmail.com','Developer',1,1,1,1);


CREATE TABLE public."EstadoSesiones" (
    "IDES" BIGINT NOT NULL,
    "ESIDEstSes" CHARACTER VARYING(1024) NOT NULL,
    "ESIDPer" BIGINT NOT NULL,
    "ESIDEst" BIGINT NOT NULL,
    "ESFechaInicio" TIMESTAMP(0) NOT NULL,
    "ESFechaFin" TIMESTAMP(0) NOT NULL,
    CONSTRAINT "PKIDES" PRIMARY KEY ("IDES"),
    CONSTRAINT "FKESIDPer" FOREIGN KEY ("ESIDPer")
    REFERENCES public."Personal" ("IDPer"),
    CONSTRAINT "FKESIDEst" FOREIGN KEY ("ESIDEst")
    REFERENCES public."Estados" ("IDEst")
);
CREATE SEQUENCE "SecES"
START WITH 1
INCREMENT BY 1
MINVALUE 1;


CREATE TABLE public."PermisosAccesos" (
    "IDPA" BIGINT NOT NULL,
    "PAIDPerm" BIGINT NOT NULL,
    "PAAcceso" CHARACTER VARYING(1024) NOT NULL,
    "PADescripcion" CHARACTER VARYING(1024) NOT NULL,
    "PAIDPers" BIGINT NOT NULL,
    "PAIDAdm" BIGINT NOT NULL,
    "PAFechaCreacion" TIMESTAMP(0) NOT NULL,
    "PAFechaActualizacion" TIMESTAMP(0) NOT NULL,
    CONSTRAINT "PKIDPA" PRIMARY KEY ("IDPA"),

    CONSTRAINT "FKPAIDPerm" FOREIGN KEY ("PAIDPerm")
    REFERENCES public."Permisos" ("IDPer"),

    CONSTRAINT "FKPAIDPers" FOREIGN KEY ("PAIDPers")
    REFERENCES public."Personal" ("IDPer"),

    CONSTRAINT "FKPAIDAdm" FOREIGN KEY ("PAIDAdm")
    REFERENCES public."Admin" ("IDAdm")
);
CREATE SEQUENCE "SecPA"
START WITH 1
INCREMENT BY 1
MINVALUE 1;











