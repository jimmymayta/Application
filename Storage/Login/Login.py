# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'login.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Login(object):
    def setupUi(self, Login):
        Login.setObjectName("Login")
        Login.resize(400, 300)
        self.LogWid = QtWidgets.QWidget(Login)
        self.LogWid.setObjectName("LogWid")
        self.LogLab1 = QtWidgets.QLabel(self.LogWid)
        self.LogLab1.setGeometry(QtCore.QRect(20, 20, 141, 31))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.LogLab1.setFont(font)
        self.LogLab1.setText("Iniciar sesión")
        self.LogLab1.setObjectName("LogLab1")
        self.LogFra = QtWidgets.QFrame(self.LogWid)
        self.LogFra.setGeometry(QtCore.QRect(20, 80, 221, 41))
        self.LogFra.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.LogFra.setFrameShadow(QtWidgets.QFrame.Raised)
        self.LogFra.setObjectName("LogFra")
        self.LogFraLab = QtWidgets.QLabel(self.LogFra)
        self.LogFraLab.setGeometry(QtCore.QRect(10, 0, 31, 31))
        self.LogFraLab.setObjectName("LogFraLab")
        self.LogFraLin = QtWidgets.QLineEdit(self.LogFra)
        self.LogFraLin.setGeometry(QtCore.QRect(70, 10, 113, 22))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(12)
        self.LogFraLin.setFont(font)
        self.LogFraLin.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.LogFraLin.setObjectName("LogFraLin")
        self.LogPus = QtWidgets.QPushButton(self.LogWid)
        self.LogPus.setGeometry(QtCore.QRect(110, 240, 181, 31))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.LogPus.setFont(font)
        self.LogPus.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.LogPus.setObjectName("LogPus")
        Login.setCentralWidget(self.LogWid)

        self.retranslateUi(Login)
        QtCore.QMetaObject.connectSlotsByName(Login)

    def retranslateUi(self, Login):
        _translate = QtCore.QCoreApplication.translate
        Login.setWindowTitle(_translate("Login", "Login"))
        self.LogFraLab.setText(_translate("Login", "TextLabel"))
        self.LogPus.setText(_translate("Login", "PushButton"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Login = QtWidgets.QMainWindow()
    ui = Ui_Login()
    ui.setupUi(Login)
    Login.show()
    sys.exit(app.exec_())

