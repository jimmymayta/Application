# -*- coding: utf-8 -*-
"""
Created on Mon May 11 00:33:39 2020
@author: Jimmy Mayta
"""

import cv2
from PyQt5 import QtCore, QtGui, QtWidgets
from Packages.Database import SQLite

class Principal(object):
    def __init__(self):
        self.cap = cv2.VideoCapture(0, cv2.CAP_DSHOW)
        self.cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1024)
        self.cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 768)
        self.Ancho = int(self.cap.get(cv2.CAP_PROP_FRAME_WIDTH))
        self.Altura = int(self.cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
        self.Font = cv2.FONT_HERSHEY_SIMPLEX
        self.Cascade = cv2.CascadeClassifier('Library/People.xml')
        self.SQL = SQLite()
        self.Title = self.SQL.Read("SELECT ConValue FROM Configurations WHERE ConConfiguration='Title'")

    def setupUi(self, Main, App):
        Main.setObjectName("Main")
        Main.setWindowIcon(QtGui.QIcon('Images/12735682.svg'))
        QSS = open('QSS/QSS.qss')
        Main.setStyleSheet(QSS.read())

        Screen = App.desktop().screenGeometry()
        ScreenWidth, ScreenHeight = Screen.width(), Screen.height()
        Width, Height = self.Ancho+100, self.Altura+100
        Main.move(((ScreenWidth / 2) - (Width / 2)), ((ScreenHeight / 2) - (Height / 2) - 100))
        Main.resize(Width, Height)
        Main.setMinimumSize(QtCore.QSize(Width, Height))

        Menu = QtWidgets.QMenuBar(Main)
        Menu.setObjectName('Menu')

        MenuArchivo = QtWidgets.QMenu(Menu)
        MenuArchivo.setObjectName('MenuArchivo')
        MenuArchivo.setTitle('Archivo')

        Help = QtWidgets.QMenu(Menu)
        Help.setObjectName('Help')
        Help.setTitle('Ayuda')

        Main.setMenuBar(Menu)
        Menu.addAction(MenuArchivo.menuAction())
        # Menu.addAction(Configuration.menuAction())
        Menu.addAction(Help.menuAction())

        exitAction = QtWidgets.QAction(QtGui.QIcon('Images/19128374.svg'), '&Salir', Menu)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.triggered.connect(QtWidgets.qApp.quit)

        Inf = QtWidgets.QAction(QtGui.QIcon('Images/25498734.svg'), '&Información', Menu)
        Inf.triggered.connect(self.Information)

        MenuArchivo.addAction(exitAction)
        Help.addAction(Inf)

        self.Widget = QtWidgets.QWidget(Main)
        self.Widget.setObjectName("Widget")

        Main.setCentralWidget(self.Widget)
        self.retranslateUi(Main)
        QtCore.QMetaObject.connectSlotsByName(Main)

    def Video(self):
        Res, Video = self.cap.read(0)
        Datos1 = Datos2 = self.Cascade.detectMultiScale(Video)
        for (x1, y1, w1, h1) in Datos1:
            for (x2, y2, w2, h2) in Datos2:
                D1, D2 = (x1, y1, (x1 + w1), (y1 + h1)), (x2, y2, (x2 + w2), (y2 + h2))
                cv2.putText(Video, "", (x1, y1 - 8), self.Font, 1, (0, 255, 0), 4, cv2.LINE_AA)
                if D1[0] == D2[0] and D1[1] == D2[1] and D1[2] == D2[2] and D1[3] == D2[3]:
                    Text, Color = "Persona", (0, 255, 0)
                elif ((D1[0] in range(D2[0], D2[2])) or (D1[2] in range(D2[0], D2[2]))) and \
                     ((D1[1] in range(D2[1], D2[3])) or (D1[3] in range(D2[1], D2[3]))):
                    Text, Color = "Cuidado", (0, 0, 255)

            cv2.putText(Video, text=Text, org=(x1, y1 - 8), fontFace=self.Font, fontScale=1, color=Color, thickness=2, lineType=cv2.LINE_8)
            cv2.rectangle(Video, (x1, y1), ((x1 + w1), (y1 + h1)), Color, 2)
        Image = QtGui.QImage(Video, Video.shape[1], Video.shape[0], Video.shape[1] * Video.shape[2], QtGui.QImage.Format_RGB888)
        Pixmap = QtGui.QPixmap()
        Pixmap.convertFromImage(Image.rgbSwapped())
        self.Label.setPixmap(Pixmap)

    def retranslateUi(self, Main):
        _translate = QtCore.QCoreApplication.translate
        Main.setWindowTitle(_translate("Main", self.Title[0][0]))

    def Data(self):
        if "True" == self.Con1[0][0]:
            Query = "UPDATE Configurations SET ConValue='False' WHERE ConConfiguration='Con1'"
            self.SQL.Update(Query)
            print("False")
        else:
            Query = "UPDATE Configurations SET ConValue='True' WHERE ConConfiguration='Con1'"
            self.SQL.Update(Query)
            print("True")

    def Information(self):
        Message = QtWidgets.QMessageBox()
        Message.setWindowIcon(QtGui.QIcon('Images/25498734.svg'))
        Message.setWindowTitle("Información")
        Message.setIconPixmap(QtGui.QIcon('Images/25498734.svg').pixmap(48))
        Message.setText("Para más información consulte:\nhttps://gitlab.softwarelibre.gob.bo/JimmyMayta/control-inteligente-de-distancia.git")
        Message.setStandardButtons(QtWidgets.QMessageBox.Yes)
        Message.show()
        Message.exec_()














