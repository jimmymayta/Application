# -*- coding: utf-8 -*-
"""
Created on Mon May 11 00:33:39 2020
@author: Jimmy Mayta
"""

import sqlite3

class SQLite:
    def __init__(self):
        self.Con = sqlite3.connect('Database/Database.db')
        self.Cur = self.Con.cursor()

    def Create(self, Query):
        self.Cur.execute("{}".format(Query))
        self.Con.commit()

    def Read(self, Query):
        Dat = self.Cur.execute(Query)
        return Dat.fetchall()

    def Delete(self, Query):
        self.Create(Query)

    def Update(self, Query):
        self.Create(Query)

class Postgres:
    def __init__(self):
        self.Con = sqlite3.connect('Database/Database.db')
        self.Cur = self.Con.cursor()

    def Create(self, Query):
        self.Cur.execute("{}".format(Query))
        self.Con.commit()

    def Read(self, Query):
        Dat = self.Cur.execute(Query)
        return Dat.fetchall()

    def Delete(self, Query):
        self.Create(Query)

    def Update(self, Query):
        self.Create(Query)













